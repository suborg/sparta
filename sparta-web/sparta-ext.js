/**
 * SPARTA: Stack Phone Application RunTime Accelerator
 * JS implementation
 * @version 0.0.1
 * @author Luxferre
 * @license Unlicense <https://unlicense.org>
 */

//sparta-ext
//Extension setup according to the main spec

function SpartaExtensions() {

  //system device (0x0)
  function setupSys(vm, configMatrix) {
    var profileVal = 0, profileKeys = [
      'voiceenabled',
      'headsetaction',
      'flipaction',
      'network',
      'file',
      'camera',
      'audio',
      'color'
    ], i

    for(i=0;i<8;i++)
      if(profileKeys[i] in configMatrix)
        profileVal |= (1<<Number(configMatrix[profileKeys[i]]))
    
    vm.setdev(0x05, profileVal)
    if('keypad' in configMatrix) {
      vm.setdev(0x06, configMatrix.keypad[0])
      vm.setdev(0x07, configMatrix.keypad[1])
      vm.setdev(0x08, configMatrix.keypad[2])
      vm.setdev(0x09, configMatrix.keypad[3])
    }
    vm.setdev(0x0a, configMatrix.screenWidth >> 8)
    vm.setdev(0x0b, configMatrix.screenWidth&255)
    vm.setdev(0x0c, configMatrix.screenHeight >> 8)
    vm.setdev(0x0d, configMatrix.screenHeight&255)
    vm.config = configMatrix

    //setup RNG
    var getRandomByte = ('crypto' in window) ? function() {
      return window.crypto.getRandomValues(new Uint8Array(1))[0]
    } : function() {return Math.random()*256|0}

    vm.setReadHook(0x0e, function() { //update the random byte port before the program consumes it
      vm.setdev(0x0e, getRandomByte())
    })

  }

  //standard I/O (0x10)
  function setupStdio(vm) {
    var stdoutbuf = '', stderrbuf = ''
    vm.setWriteHandler(0x10, function(port, val) {
      var fmt = vm.getdev(0x1a),
          bufOut = fmt&1,
          bufErr = fmt&4,
          hexOut = fmt&2,
          hexErr = fmt&8,
          fmtVal

      if(port === 0x18) {
        fmtVal = hexOut ? ('0' + val.toString(16)).slice(-2) + ' ' : String.fromCharCode(val)
        if(bufOut) {
            if(val === vm.config.stdoutBufferEnd) {
              console.log(stdoutbuf)
              stdoutbuf = ''
            }
            else stdoutbuf += fmtVal
        }
        else console.log(fmtVal)
      } else if(port === 0x19) {
        fmtVal = hexErr ? ('0' + val.toString(16)).slice(-2) + ' ' : String.fromCharCode(val)
        if(bufErr) {
            if(val === vm.config.stdoutBufferEnd) {
              console.error(stderrbuf)
              stderrbuf = ''
            }
            else stderrbuf += fmtVal
        }
        else console.error(fmtVal)
      }
    })
  }

  //graphics/text (0x20)
  function setupScreen(vm, cnv) {
    var width = vm.config.screenWidth,
        height = vm.config.screenHeight,
        mono = !vm.config.color,
        ctx = cnv.getContext('2d', {alpha: false}),
        portbuf = new Uint8Array(16),
        fontProfiles = [
          '12px monospace' //0x00, default, at least one needs to be here
        ]
    ctx.globalAlpha = 1

    vm.setWriteHandler(0x20, function(port, val) {
      var portidx = port&15
      if(portidx === 2) { //implement draw command
        var doChar = val&4, //bit 2 - draw character
            doSprite = val&2, //bit 1 - draw sprite
            doPixel = val&1, //bit 0 - draw pixel
            doClear = val&128, //bit 7 - clear screen before drawing
            doFill = val&64, //bit 6 - fill screen before drawing
            dir = (val&32) ? -1 : 1, //bit 5 - invert cursor auto movement
            doVertAutoMovement = val&16, //bit 4 - enable vertical cursor auto movement
            doHorizAutoMovement = val&8, //bit 3 - enable horizontal cursor auto movement
            selectedFont = fontProfiles[portbuf[12]],
            xCoord = (portbuf[3]<<8)|portbuf[4],
            yCoord = (portbuf[5]<<8)|portbuf[6],
            pxlOffset = yCoord * width + xCoord,
            colorVals = [portbuf[7], portbuf[8], portbuf[9]],
            charSpriteVal = (portbuf[10]<<8)|portbuf[11], //the character codepoint or the sprite address depending on the mode
            screendata = ctx.getImageData(0, 0, width, height),
            multiplierH = 1, multiplierW = 1, //auto shift change based on what we draw (defaulting to single-pixel size)
            i, j, totalRGBA=screendata.data.length //loop indexes and other vars
        if(mono && (colorVals[0]>0 || colorVals[1]>0 || colorVals[2]>0)) //ensure monochrome logic emulation even on HTML5
          colorVals = [255, 255, 255]
        function pxl(i, clr) { //pixel draw helper 
          screendata.data[i] = (clr ? 0 : colorVals[0])
          screendata.data[i + 1] = (clr ? 0 : colorVals[1])
          screendata.data[i + 2] = (clr ? 0 : colorVals[2])
          screendata.data[i + 3] = 255
        }
        //start the drawing process
        //first, determine if we need to clear or fill the screen
        if(doClear || doFill) {
          for(i=0;i<totalRGBA;i+=4)
            pxl(i, !doFill)
          ctx.putImageData(screendata, 0, 0) //fill in screen data
        }
        //if we filled the screen, there's no sense to draw anything with the same color
        if(!doFill) { //otherwise perform the actual drawing if and only if one of the corresponding bits is set
          if(doChar) { //text drawing bit has the highest precedence
            ctx.font = selectedFont
            ctx.textAlign = 'left'
            ctx.textBaseline = 'top'
            ctx.fillStyle = 'rgba(' + colorVals.join(',') + ', 1)'
            var textBuf = String.fromCharCode(charSpriteVal),
                meas = ctx.measureText(textBuf)
            multiplierH = meas.height
            multiplierW = meas.width
            ctx.fillText(textBuf, xCoord, yCoord)
          } else if(doSprite) { //sprites have the medium precedence
            multiplierH = 8
            multiplierW = 8
            spriteArr = vm.getram(charSpriteVal, 8) //now we have a 8x8 bitmap
            for(j=0;j<8;j++) { //go over sprite rows
              for(i=0;i<8;i++) { //go over sprite pixels in each row
                if((spriteArr[j] >> (7-i))&1) { //bit found, light up the pixel
                  pxl((pxlOffset+j*width+i)<<2)
                }
              }
            }
          } else if(doPixel) { //pixels have the lowest precedence
            pxl(pxlOffset<<2)
          }
        }
        //finaly, re-render the screen data
        if(doSprite || doPixel)
          ctx.putImageData(screendata, 0, 0)
        //now let's move the cursor if necessary
        if(doHorizAutoMovement) {
          xCoord += dir*multiplierW
          if(xCoord > width - 1) xCoord -= width
          else if(xCoord < 0) xCoord += width
          portbuf[3] = xCoord >> 8
          portbuf[4] = xCoord&255
          vm.setdev(0x23, portbuf[3])
          vm.setdev(0x24, portbuf[4])
        }
        if(doVertAutoMovement) {
          yCoord += dir*multiplierH
          if(yCoord > height - 1) yCoord -= height
          else if(yCoord < 0) yCoord += height
          portbuf[5] = yCoord >> 8
          portbuf[6] = yCoord&255
          vm.setdev(0x25, portbuf[5])
          vm.setdev(0x26, portbuf[6])
        }
      }
      else //just write the portbuf values to the corresponding place
        portbuf[portidx] = val
    })

    // also, we need to set up the screen vector

    var rAF = window.requestAnimationFrame || window.webkitRequestAnimationFrame
    rAF(function vecframe() {
      vm.triggerVector(0x20)
      if(vm.active)
        rAF(vecframe)
    })

  }

  //keypad input (0x80)

  var livemap = { //map keys to bit values (to be updated in one of 4 ports)
      '0': 0x00,
      '1': 0x01,
      '2': 0x02,
      '3': 0x03,
      '4': 0x04,
      '5': 0x05,
      '6': 0x06,
      '7': 0x07,
      '8': 0x08,
      '9': 0x09,
      '*': 0x0a,
      '#': 0x0b,
      'ArrowUp': 0x0c,
      'ArrowDown': 0x0d,
      'ArrowLeft': 0x0e,
      'ArrowRight': 0x0f,
      'SoftLeft': 0x10,
      'SoftRight': 0x11,
      'Enter': 0x12, //CSK
      'Call': 0x13,
      'End': 0x14,
      'Notification': 0x15, //Menu
      'Backspace': 0x16, //Back
      'Camera': 0x17,
      'VolumeUp': 0x18,
      'VolumeDown': 0x19,
      'Power': 0x1a,
      'Help': 0x1b, //assist key
      //others are currently unsupported but we need to define flip/headset action flags here
      'Light': 0x1c,
      'Flip': 0x1d,
      'HS': 0x1e,
      'Aux': 0x1f
    }

  //set KaiOS aliases if necessary
  livemap['EndCall'] = livemap['End']

  //now remap the keys for PC according to the spec recommendation
  
  livemap['q'] = livemap['Aux']
  livemap['h'] = livemap['HS']
  livemap['f'] = livemap['Flip']
  livemap['l'] = livemap['Light']
  livemap['a'] = livemap['Assist']
  livemap['p'] = livemap['Power']
  livemap[','] = livemap['VolumeDown']
  livemap['.'] = livemap['VolumeUp']
  livemap['c'] = livemap['Camera']
  livemap['m'] = livemap['Menu']
  livemap['x'] = livemap['End']
  livemap[' '] = livemap['Call']
  livemap[']'] = livemap['SoftRight']
  livemap['['] = livemap['SoftLeft']

  function setupInput(vm) {

    function keypress(e) {
      var k = e.key
      if(k in livemap) {
        var keyval = livemap[k],
            portnum = 0x85 - (keyval >>> 3),
            supportval = vm.getdev(0x09 - (keyval >>> 3)),
            bitmask = 1<<(keyval&7)
        vm.setdev(portnum, (vm.getdev(portnum) | bitmask) & supportval)
        vm.triggerVector(0x80)
      }
    }
    function keyrelease(e) {
      var k = e.key
      if(k in livemap) {
        var keyval = livemap[k],
            portnum = 0x85 - (keyval >>> 3),
            bitmask = (~(1<<(keyval&7)))&255
        vm.setdev(portnum, vm.getdev(portnum) & bitmask)
        vm.triggerVector(0x80)
      }
    }
    window.removeEventListener('keydown', keypress)
    window.removeEventListener('keyup', keyrelease)
    window.addEventListener('keydown', keypress)
    window.addEventListener('keyup', keyrelease)

    //todo: add actual headset and flip/slide switches

    vm.setdev(0x82, 0)
    vm.setdev(0x83, 0)
    vm.setdev(0x84, 0)
    vm.setdev(0x85, 0)
  }

  //datetime (0xc0) - read-only for this implementation

  function setupDatetime(vm) {
    //thanks to Toastrackenigma for the idea
    function isDST(d, y) {
      var jan = new Date(y, 0, 1).getTimezoneOffset(),
          jul = new Date(y, 6, 1).getTimezoneOffset()
      return Math.max(jan, jul) !== d.getTimezoneOffset() 
    }
    //thanks to user2501097 for the idea
    function daysIntoYear(y, m, d) {
      return (Date.UTC(y, m, d) - Date.UTC(y, 0, 0)) / 86400000
    }
    function fillDTports() {
      var now = new Date(),
          year = now.getFullYear(),
          month = now.getMonth() + 1,
          day = now.getDate(),
          hour = now.getHours(),
          minute = now.getMinutes(),
          second = now.getSeconds(),
          dotw = now.getDay(),
          doty = daysIntoYear(year, month - 1, day),
          dstflag = 0|isDST(now, year)
      vm.setdev(0xc0, year >> 8)
      vm.setdev(0xc1, year&255)
      vm.setdev(0xc2, month)
      vm.setdev(0xc3, day)
      vm.setdev(0xc4, hour)
      vm.setdev(0xc5, minute)
      vm.setdev(0xc6, second)
      vm.setdev(0xc7, dotw)
      vm.setdev(0xc8, doty >> 8)
      vm.setdev(0xc9, doty&255)
      vm.setdev(0xca, dstflag)
    }
    if(window.globalDTInterval) {
      window.clearInterval(window.globalDTInterval)
      window.globalDTInterval = null
    }
    window.globalDTInterval = window.setInterval(fillDTports, 1000)
  }

  //unimplemented yet
  function setupEvents(vm) {}
  function setupIntf(vm) {}
  function setupFile(vm) {}
  function setupAudio(vm) {}

  return {
    setup: function(vm, cfg) {
      setupSys(vm, cfg)
      setupStdio(vm)
      setupScreen(vm, cfg.canvas)
      setupInput(vm)
      setupDatetime(vm)
      //setupEvents(vm)
      //setupIntf(vm)
      //setupFile(vm)
      //setupAudio(vm)
    }
  }
}

