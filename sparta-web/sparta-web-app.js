//app part not related to Sparta JS implementation

function main() {
  var devCfg = { //reference device config as in Nokia 2720 Flip but with QWERTY
    screenWidth: 240, //ports 0x0a-0x0b
    screenHeight: 320, //ports 0x0c-0x0d
    stdoutBufferEnd: 0x0d,
    //port 0x05 - device profile:
    color: true, //bit 7
    audio: true, //bit 6
    camera: true, // bit 5
    file: true, //bit 4
    network: false, //bit 3
    flipaction: true, //bit 2
    headsetaction: true, //bit 1
    voiceenabled: true, //bit 0
    keypad: [ //keypad support
      0b11101111, //0x06
      0b01011111, //0x07
      0xff, //0x08
      0xff //0x09
    ],
    canvas: document.getElementById('C') //JS-specific implementation canvas DOM reference
  }

  document.getElementById('apprun').addEventListener('click', function() {
    var fileObj = document.getElementById('appselect').files[0]
    if(fileObj.name.endsWith('.usp')) {
      var rdr = new FileReader()
      rdr.onload = function() {
        var vm = SpartaVM()
        var ext = SpartaExtensions()
        vm.boot()
        ext.setup(vm, devCfg)
        vm.load(new Uint8Array(rdr.result))
        console.log('VM started')
        vm.start(0x100)
      }
      rdr.readAsArrayBuffer(fileObj)
    }
    else window.alert('Please select a .usp file to run on SPARTA')
  })
}

addEventListener('DOMContentLoaded', main)
