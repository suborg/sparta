# SPARTA: Stack Phone Application RunTime Accelerator

SPARTA is a platform-agnostic runtime environment to write lightweight feature phone applications with small energy footprint. Using SPARTA engine as the middleware, these applications can run unmodified on any phone OS the engine is ported to: just like J2ME but even simpler. Primary development targets include:

- Web/HTML5 (JS reference implementation, prototyping);
- KaiOS/HTML5 (packaged JS implementation with KaiOS-specific backend features);
- Bananian/wbananui (ANSI C reference implementation).

Secondary targets for further development:

- MAUI/MRE (MediaTek feature phones);
- Mocor RTOS (Unisoc feature phones);
- ZeroPhone (Pi Zero + SIM800 feature phone).

SPARTA implements a [Uxn](https://wiki.xxiivv.com/site/uxn.html) stack VM with its own phone-specific extensions only partially compatible with the [Varvara](https://wiki.xxiivv.com/site/varvara.html) machine. This means SPARTA programs (usually written in Uxntal language directly, further referred to as Uxn/SPARTA to differentiate the runtime from Uxn/Varvara) still can be compiled with `uxnasm` and perform console I/O when run with `uxncli`, but the GUI won't work correctly when run with `uxnemu`, and some other specs like Audio or MIDI aren't implemented at all. To test Uxn/SPARTA applications with GUI, you will need to use the `sparta-web/sparta-web.html` reference interpreter shipped within this repo. To make applications easier to develop with these non-standard specs, SPARTA also provides its own macro library in Uxntal language for defining and using standardized phone UI components and vectors for event-driven programming.

Uxn/SPARTA apps can have any file suffix, but the convention is to have `.tal` for the source code in Uxntal and `.usp` for the app binaries.

## Main SPARTA components

1. The core (`sparta-core`). Must be reimplemented for every target in its native language. The core is the virtual CPU of SPARTA, a plain unmodified Uxn VM that must be fully compatible with the reference Uxn implementation.
2. The extension backend (`sparta-ext`). Must be reimplemented for every target in its native language. Defines the way Uxn/SPARTA programs communicate with the outer world using `DEI` and `DEO` opcodes. See below for the `sparta-ext` specification and implementation requirements.
3. The standard library (`sparta-lib.tal` for macros and `sparta-sr.tal` for subroutines). Implemented in Uxntal and reusable across targets. Requires `sparta-ext` to work. Defines a set of macros and subroutines for general-purpose and GUI programming for SPARTA runtime.

## Extension backend specification

The following Uxn devices must be implemented by `sparta-ext` as stated below. The rest of the ports in these devices can be backend-specific (although it is discouraged) or omitted altogether.

Current state of device implementation in `sparta-web` reference/prototyping runtime:

- `0x00`: yes
- `0x10` (stdio): standard output and standard error only
- `0x20` (screen): yes
- `0x30-0x60` (audio output): no 
- `0x70` (audio input): no 
- `0x80` (keypad input): yes
- `0x90` (external serial I/O): no
- `0xa0`-`0xb0` (file I/O): no 
- `0xc0` (datetime): yes, read-only 
- `0xd0` (image input): no
- `0xe0` (system events): no
- `0xf0` (system interfaces): no

### System device (0x00)

Extensions must implement the halting port `0x0f` and the trace/random number source port `0x0e`, as well as `0x05` byte and `0x06-0x07`, `0x08-0x09`, `0x0a-0x0b` and `0x0c-0x0d` system configuration shorts.

Unlike Uxn/Varvara runtime, Uxn/SPARTA explicitly prohibits usage of `wst`/`rst` ports (`0x02` and `0x03`) on the architectural level since main RAM and stack memory areas are always to be separated. The developer can write to these ports or read from them but the runtime must not react to any actions performed on them.

SPARTA-specific configuration shorts to be pre-filled by the runtime engine to be ready for usage in the apps:

- `0x05`: device profile byte;
- `0x06-0x07` and `0x08-0x09`: input compatibility matrix;
- `0x0a-0x0b`: device screen width;
- `0x0c-0x0d`: device screen height.

Device profile bits (from 7 to 0) are to be set if the following capabilities are supported:

7    |6        |5     |4       |3          |2                  |1              |0
-----|---------|------|--------|-----------|-------------------|---------------|---------------------
Color|Audio I/O|Camera|File I/O|Network I/O|Flip/slider control|Headset control|Telephony (voice/SMS)

Input compatibility matrix contains of two shorts, or 32 bits, and allows the app to unambiguously determine which input keys are supported by the device. This is the list of all keys mapped to compatibility ports:

port|7     |6   |5   |4    |3     |2    |1   |0
----|------|----|----|-----|------|-----|----|----
0x06|Aux   |HS  |Flip|Light|Assist|Power|Vol-|Vol+
0x07|Camera|Back|Menu|End  |Call  |CSK  |RSK |LSK
0x08|Right |Left|Down|Up   |`#`   |`*`  |9   |8
0x09|7     |6   |5   |4    |3     |2    |1   |0

Implementation notes:

- `HS` (headset inserted) and `Flip` (flip open) bits, if supported, are to be updated by the runtime on every input event.
- `Light` denotes a dedicated flashlight key present on some phones, but doesn't apply to the devices with hardware flashlight trigger/button switch.
- `Aux` key functionality may change depending on the connected peripherals. For instance, if a wired headset is connected, the runtime may map the headset button to the `Aux` key.

Extensions should implement `0x00-0x01` vector to be triggered on halt or on the configuration variable change.

Note: when designing an app for SPARTA platform, the developer can only use ports `0x05-0x0d` as the only source of truth to adjust the app logic. On different devices with the same profile, compatibility matrix, screen width and screen height values, a Uxn/SPARTA application must behave in an identical way. The `0x05-0x0d` ports can also be written to (and it can't be prevented), but the values stored there may be overwritten by the engine at any time.

For the `0x0e` port that can be written to to display a stack trace in the runtime but must return a random byte value from 0 to 255 when read from, it's entirely up to the implementation to choose the randomness source, to implement a truly random or a secure enough pseudorandom generator. From the Uxn/SPARTA application standpoint, the value obtained from the `0x0e` port must be considered truly random and not derived from anything else.

### Standard input/output (0x10)

Extensions must implement `0x18` and `0x19` ports for standard output and standard error streams, as well as `0x1a` port for output format configuration (see below).

Extensions may implement target-specific standard input with `0x12` port and `0x10-0x11` vector, but they must only be used for debugging purposes and the main application logic must never depend on them.

By default, to maintain compatibility with existing Uxn test suites, the output to both streams must be raw and unbuffered. To change this behavior from the app, the runtime must implement the `0x1a` output config port that accepts a 4-bit value. The bits are as follows:

- bit 3: set the formatting mode of the error stream (0 - raw, 1 - formatted);
- bit 2: set the buffering mode of the error stream (0 - unbuffered, 1 - buffered);
- bit 1: set the formatting mode of the output stream (0 - raw, 1 - formatted);
- bit 0: set the buffering mode of the output stream (0 - unbuffered, 1 - buffered).

This specification doesn't explicitly state how the buffered and/or formatted output must look, it's entirely up to the target implementation. The reference Web implementation, `sparta-web`, implements these as follows:

- buffering mode bit, if set, makes the ports wait for the `0x0d` byte to be sent before actually outputting the characters to the JS console, and if unset, the characters are output individually;
- formatting mode bit, if set, makes the ports output a hexadecimal-formatted value and a whitespace.

Note: if the output mode is changed from buffered to unbuffered and the buffer hasn't been flushed by meeting the output condition, the implementation can either discard its contents or preserve them until the next buffered output. This is considered undefined behavior and should be avoided in the Uxn/SPARTA application logic. Always make sure you meet the buffer output condition before changing the output mode.

### Graphics/text output (0x20)

SPARTA's screen output is fully incompatible with Varvara or other existing Uxn runtimes. The screen output capabilities are determined by:

- screen width (exposed to apps at `0x0a-0x0b` port);
- screen height (exposed to apps `0x0c-0x0b` port);
- mono/color flag (exposed to apps at bit 7 of the device profile port `0x05`).

Port layout of SPARTA screen device:

- `0x20-0x21`: vector (triggered 60 times per second);
- `0x22`: draw command (see below);
- `0x23-0x24`: set/get X coordinate;
- `0x25-0x26`: set/get Y coordinate;
- `0x27-0x29`: color ports (R, G, B);
- `0x2a-0x2b`: character/sprite port;
- `0x2c`: font selector.

Draw command bits:

- bit 7: clear the screen before drawing (reset all pixel colors to 0,0,0);
- bit 6: fill the entire screen with the value from the color port before drawing;
- bit 5: invert the cursor auto-movement;
- bit 4: automatically move the cursor vertically down (or up if bit 5 is set) after drawing;
- bit 3: automatically move the cursor horizontally to the right (or left if bit 5 is set) after drawing ;
- bit 2: draw a character with the codepoint taken from the character/sprite port and with the color taken from the color ports;
- bit 1: draw a 8x8 sprite from the address taken from the character/sprite port and with the color taken from the color ports;
- bit 0: draw a pixel with the color taken from the color ports.

Of the bits 2, 1 and 0, only one can be set at a time. If several of them are set, bit 2 takes precedence over bits 1 and 0 and bit 1 takes precedence over bit 0.
The cursor auto-movement behavior also depends on what we're trying to output. If it's a pixel, the cursor will move 1 pixel in the set direction, if it's a sprite, the cursor will move 8 pixels, if it's a character, it's up to the backend to decide on the optimal movement offset.

For monochrome output, the following applies:
- any non-zero value in any of the `0x27-0x29` ports should light up a pixel;
- if the `0x27-0x29` ports are all zero, setting bit 6 in the draw command doesn't differ from setting bit 7, otherwise it will light up all pixels on the screen.

The character port supports setting 16-bit codepoints, potentially allowing for Unicode support with basic 65535 characters. However, the Uxn/SPARTA runtime extensions are only required to support the first 127 character output. To set ASCII values, keep the `0x2a` port at zero and only set `0x2b` port to the required 8-bit value.

The same port can also be used as a sprite port, where the address of a 8-byte sprite is stored. The extensions must read 8 bytes starting from the specified address and render them as 8x8 bitmap with the currently selected color.

The font selector port accepts a byte with higher 4-bit nibble selecting a base font from up to 16 preinstalled fonts, and lower nibble selecting its size from up to 16 sizes. If this port isn't written to, the `0x00` value is assumed. Extensions must provide at least one default font of the default size at the `0x00` font selector byte.

Note that SPARTA doesn't support greyscale output or any color profiles other than RGB and true monochrome on the engine level. If you need to use greyscale or anything else on the target device, you must report the device as color in the device profile values and then do the necessary conversions in the engine implementation itself.

The `0x20-0x21` vector to be triggered when drawing/printing outside the screen boundaries is optional but recommended to implement.

### Audio output (0x30-0x60)

(under construction)

### Audio input (0x70)

(under construction)

### Keypad input (0x80)

On SPARTA platform, extensions must implement the input vector (`0x80-0x81`) and four controller ports (`0x82` to `0x85`). All controller port value bits are changed on the input events (key press or release) where applicable, and the vector is triggered.

Extensions must implement the following controller input keys based on the device input compatibility matrix populated from the system port:

port|7     |6   |5   |4    |3     |2    |1   |0
----|------|----|----|-----|------|-----|----|----
0x82|Aux   |HS  |Flip|Light|Assist|Power|Vol-|Vol+
0x83|Camera|Back|Menu|End  |Call  |CSK  |RSK |LSK
0x84|Right |Left|Down|Up   |`#`   |`*`  |9   |8
0x85|7     |6   |5   |4    |3     |2    |1   |0

This support must be exclusive, i.e. if the runtime does not report the support of a particular key in the ports `0x06` to `0x09`, it also must not set corresponding bits in the ports `0x82` to `0x85`. Think of the input compatibility matrix as the bitmask to be AND-ed before processing any input.

Notes: See also the system device section for the implementation notes. The `HS` and `Flip` bits, if supported, are to be constantly set as long as the corresponding condition is met (headset inserted or flip open accordingly).

In addition to this spec, desktop-based development tools and emulators (such as `sparta-web`) should map the phone keys available via ports `0x82` and `0x83` to the PC keyboard as follows:

Phone key|Aux|HS|Flip|Light|Assist|Power|Vol-  |Vol+
---------|---|--|----|-----|------|-----|------|----
PC key   |Q  |H |F   |L    |A     |P    |`,`   |`.` 

Phone key|Camera|Back     |Menu|End|Call |CSK  |RSK|LSK
---------|------|---------|----|---|-----|-----|---|---
PC key   |C     |Backspace|M   |X  |Space|Enter|`]`|`[`

### External serial I/O (0x90)

(under construction)

### File devices (0xa0, 0xb0)

(under construction)

### Datetime device (0xc0)

Datetime ports are read/write and fully compatible with Uxn/Varvara runtime:

- `0xc0-0xc1`: year
- `0xc2`: month
- `0xc3`: day
- `0xc4`: hour
- `0xc5`: minute
- `0xc6`: second
- `0xc7`: day of the week
- `0xc8-0xc9`: day of the year
- `0xca`: DST flag for the local time

The implementations may limit the possibility of overwriting the actual datetime information depending on the target platform.

### Image input (0xd0)

(under construction)

### System events (0xe0)

The SPARTA runtime may define up to 256 different system event types, and the applications may set up to 4 non-zero vectors to listen to these events. The ports are as follows:

- `0xe0-0xe1`: event vector 1;
- `0xe2-0xe3`: event vector 2;
- `0xe4-0xe5`: event vector 3;
- `0xe6-0xe7`: event vector 4;
- `0xe8`: event type 1;
- `0xe9`: event type 2;
- `0xea`: event type 3;
- `0xeb`: event type 4;
- `0xec`: emit/simulate a system event of the given type.

System events are supposed to populate the values into the system interface ports `0xf0` to `0xf7`. For the convenience, short (2-byte) parameters will have the `ptr` suffix below.

The system events implementation may or may not duplicate the events already handled by other devices in this specification. Particular system event types are not enforced here or required for implementation, but the recommended type values for a generic phone environment are:

type  |capability|parameters in `0xf0-0xf7`                |meaning
------|----------|-----------------------------------------|-------
`0xb0`|Battery   |status                                   |Battery charge status changed, the status byte is set to the value of `0xfb` port (see below)
`0xb1`|Bluetooth |(reserved)                               |Reserved for future Bluetooth-specific events
`0xc0`|Telephony |mode (0-5), simid, callid, numptr        |voice call event (modes: incoming, initiated, connected, disconnected, on hold, bridged)
`0xc1`|Telephony |mode (0-2), simid, msgid, numptr, textptr|SMS event (modes: incoming, sent, delivered)
`0xc2`|Telephony |mode (0-1), simid, msgid, numptr, textptr|USSD event (modes: incoming, response-sent)
`0xc3`|Telephony |mode (0-3), simid, mcc, mnc, nameptr     |Network status change (modes: disconnected, registering, registered, reg. failure)
`0xc4`|Telephony |mode (0-1), bufptr                       |AT string received (modes: disconnected, connected)
`0xc5`|Telephony |(reserved)                               |STK data received (reserved)

### System interfaces (0xf0)

SPARTA implements 16 ports to communicate with different phone components depending on the capabilities reported in the profile:

- `0xf0-0xf1`: command parameter 1 short;
- `0xf2-0xf3`: command parameter 2 short;
- `0xf4-0xf5`: command parameter 3 short;
- `0xf6-0xf7`: command parameter 4 short;
- `0xf8`: invoke a phonebook/call log/restriction list command (see below);
- `0xf9`: get/set current screen brightness (0 to 255);
- `0xfa`: get/set the audio channel's volume (highest 2 bits - channel ID from 0 to 3, lower 6 bits - volume value from 0 to 63);
- `0xfb`: get the battery status (highest bit: 0 - discharging, 1 - charging) and level (remaining 7 bits from 0 to 127);
- `0xfc`: invoke a telephony interface command (see below);
- `0xfd`: invoke a Bluetooth interface command (reserved);
- `0xfe`: invoke an extended set command (reserved for future specification extensions);
- `0xff`: run another Uxn/SPARTA program (see below).

(under construction)

## Standard library specification

(under construction)


## Credits

Created by Luxferre in 2022, released into public domain.

Special thanks to:

- Evil King Stan for Uxn VM suggestion for the runtime core;
- Devine Lu Linvega, Andrew Alderwick, Andrew Richards for creating Uxn VM specification and assembly tools;
- daniq111 for the naming inspiration with his "Sparta" lightweight solar vehicle prototype.
